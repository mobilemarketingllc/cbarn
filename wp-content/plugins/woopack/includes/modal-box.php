<div class="woopack-modal" style="display: none;">
    <div class="woopack-modal-overlay" style="background-image: url(<?php echo WOOPACK_URL; ?>assets/images/loader.gif);"></div>
    <div class="woopack-modal-inner">
        <div class="woopack-modal-close">×</div>
        <div class="woopack-modal-content"></div>
    </div>
</div>
