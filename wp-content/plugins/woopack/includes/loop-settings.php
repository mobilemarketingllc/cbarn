<?php

// Default Settings
$defaults = array(
	'data_source' 				=> 'custom_query',
	'post_type'   				=> 'product',
	'order_by'    				=> 'date',
	'order'       				=> 'DESC',
	'offset'      				=> '0',
	'users'       				=> '',
	'product_source'			=> 'all',
	'exclude_current'			=> 'no',
	'product_title'				=> 'yes',
	'product_price'				=> 'yes',
	'product_rating'			=> 'yes',
	'product_rating_count'			=> 'no',
	'product_short_description'	=> 'no',
	'show_sale_badge'			=> 'yes',
	'button_type'				=> 'cart',
	'button_text'				=> __( 'View More', 'woopack' ),
	'qty_input'					=> 'no',
	'show_image'				=> 'yes',
	'image_size'				=> 'medium',
	'image_slider'				=> 'no',
	'show_taxonomy'				=> 'yes',
	'taxonomy_custom_text'		=> '',
	'show_quick_view'			=> 'yes',
	'quick_view_custom_text'	=> __( 'Quick View', 'woopack' ),
	'quick_view_type'			=> 'image_hover',
	'variation_fields'			=> 'no'
);

$tab_defaults = isset( $tab['defaults'] ) ? $tab['defaults'] : array();
$settings     = (object)array_merge( $defaults, $tab_defaults, (array)$settings );
$settings 	  = apply_filters( 'woopack_loop_settings', $settings );  //Allow extension of default Values
do_action( 'woopack_loop_settings_before_form', $settings ); // e.g Add custom FLBuilder::render_settings_field()

?>
<div id="fl-builder-settings-section-source" class="fl-loop-data-source-select fl-builder-settings-section">
	<table class="fl-form-table">
		<?php

		// Data Source
		FLBuilder::render_settings_field( 'data_source', array(
			'type'          => 'select',
			'label'         => __( 'Source', 'woopack' ),
			'default'		=> 'custom_query',
			'options'       => array(
				'custom_query'  => __( 'Custom Query', 'woopack' ),
				'main_query'    => __( 'Main Query', 'woopack' ),
			),
			'toggle'        => array(
				'custom_query'  => array(
					'fields'        => array( 'posts_per_page' )
				),
				'acf_relationship'	=> array(
					'sections'			=> array( 'acf_relationship' ),
					'fields'        	=> array( 'posts_per_page' )
				)
			)
		), $settings);

		?>
	</table>
</div>

<div class="fl-loop-data-source-acf fl-loop-data-source" data-source="acf_relationship">
	<div id="fl-builder-settings-section-acf_relationship" class="fl-builder-settings-section">
		<table class="fl-form-table">
		<?php
		FLBuilder::render_settings_field( 'data_source_acf_relational_type', array(
			'type'		=> 'select',
			'label'		=> __( 'Type', 'woopack' ),
			'default'       => 'relationship',
			'options'       => array(
				'relationship'  => __( 'Relationship', 'woopack' ),
				'user'          => __( 'User', 'woopack' ),
			),
		), $settings);

		FLBuilder::render_settings_field( 'data_source_acf_relational_key', array(
			'type'          => 'text',
			'label'         => __( 'Key', 'woopack' ),
		), $settings);
		?>
		</table>
	</div>
</div>

<div class="fl-custom-query fl-loop-data-source" data-source="custom_query">
	<div id="fl-builder-settings-section-general" class="fl-builder-settings-section">
		<h3 class="fl-builder-settings-title"><?php _e( 'Custom Query', 'woopack' ); ?></h3>
		<table class="fl-form-table">
			<?php
			FLBuilder::render_settings_field( 'post_type', array(
				'type'          => 'select',
				'label'         => __( 'Post Type', 'woopack' ),
				'default'		=> 'product',
				'options'		=> array(
					'product'		=> __( 'Product', 'woopack' )
				),
			), $settings);
			FLBuilder::render_settings_field( 'order', array(
				'type'          => 'select',
				'label'         => __( 'Order', 'woopack' ),
				'options'       => array(
					'DESC'          => __( 'Descending', 'woopack' ),
					'ASC'           => __( 'Ascending', 'woopack' ),
				),
			), $settings);
			FLBuilder::render_settings_field( 'order_by', array(
				'type'          => 'select',
				'label'         => __( 'Order By', 'woopack' ),
				'options'       => array(
					'author'         => __( 'Author', 'woopack' ),
					'comment_count'  => __( 'Comment Count', 'woopack' ),
					'date'           => __( 'Date', 'woopack' ),
					'modified'       => __( 'Date Last Modified', 'woopack' ),
					'ID'             => __( 'ID', 'woopack' ),
					'menu_order'     => __( 'Menu Order', 'woopack' ),
					'meta_value'     => __( 'Meta Value (Alphabetical)', 'woopack' ),
					'meta_value_num' => __( 'Meta Value (Numeric)', 'woopack' ),
					'rand'        	 => __( 'Random', 'woopack' ),
					'title'          => __( 'Title', 'woopack' ),
					'post__in'       => __( 'Selection Order', 'woopack' ),
				),
				'toggle'		=> array(
					'meta_value' 	 => array(
						'fields' => array( 'order_by_meta_key' ),
					),
					'meta_value_num' => array(
						'fields' => array( 'order_by_meta_key' ),
					)
				)
			), $settings);
			FLBuilder::render_settings_field( 'order_by_meta_key', array(
				'type'        => 'text',
				'label'       => __( 'Meta Key', 'woopack' ),
				'help' 		  => sprintf(
					__( 'WooCommerce product meta keys: %s for Price, %s for Rating, %s for Number of Reviews, %s for Number of Sales, %s for Stock Status, %s for Stock quantity', 'woopack' ),
					'<br><strong>_price</strong>',
					'<br><strong>_wc_average_rating</strong>',
					'<br><strong>_wc_review_count</strong>',
					'<br><strong>_total_sales</strong>',
					'<br><strong>_stock_status</strong>',
					'<br><strong>_stock</strong>'
				),
			), $settings);
			FLBuilder::render_settings_field( 'offset', array(
				'type'          => 'text',
				'label'         => _x( 'Offset', 'How many posts to skip.', 'woopack' ),
				'default'       => '0',
				'size'          => '4',
				'help'          => __( 'Skip this many posts that match the specified criteria.', 'woopack' )
			), $settings);
			?>
		</table>
	</div>
	<div id="fl-builder-settings-section-product-settigns" class="fl-builder-settings-section">
		<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Product Settings', 'woopack' ); ?></h3>
		<table class="fl-form-table">
			<?php
			FLBuilder::render_settings_field( 'product_source', array(
				'type'          => 'select',
				'label'         => __( 'Products Source', 'woopack' ),
				'default'       => 'all',
				'options'       => array(
					'all'			=> __( 'All Products', 'woopack' ),
					'featured'		=> __( 'Feaured Products', 'woopack' ),
					'best_selling'	=> __( 'Best Selling Products', 'woopack' ),
					'sale'			=> __( 'Sale Products', 'woopack' ),
					'top_rated'		=> __( 'Top Rated Products', 'woopack' ),
					'related'		=> __( 'Related Products', 'woopack' ),
				),
			), $settings);
			?>
			<?php
			FLBuilder::render_settings_field( 'exclude_current', array(
				'type'          => 'select',
				'label'         => __( 'Exclude Current Product', 'woopack' ),
				'default'       => 'no',
				'options'       => array(
					'yes'			=> __( 'Yes', 'woopack' ),
					'no'			=> __( 'No', 'woopack' ),
				),
			), $settings);
			?>
		</table>
	</div>
	<div id="fl-builder-settings-section-filter" class="fl-builder-settings-section">
		<h3 class="fl-builder-settings-title"><?php _e( 'Filter', 'woopack' ); ?></h3>
		<table class="fl-form-table fl-custom-query-filter fl-custom-query-product-filter" <?php if( 'product' == $settings->post_type) echo 'style="display:table;"'; ?>>
			<?php
			FLBuilder::render_settings_field( 'posts_product', array(
				'type'          => 'suggest',
				'action'        => 'fl_as_posts',
				'data'          => 'product',
				'label'         => __( 'Products', 'woopack' ),
				'help'          => __( 'Enter a list of Products.', 'woopack' ),
				'matching'      => true
			), $settings );
			$taxonomies = FLBuilderLoop::taxonomies( 'product' );
			foreach($taxonomies as $tax_slug => $tax) {
				FLBuilder::render_settings_field( 'tax_product' . '_' . $tax_slug, array(
					'type'          => 'suggest',
					'action'        => 'fl_as_terms',
					'data'          => $tax_slug,
					'label'         => $tax->label,
					'help'          => sprintf( __( 'Enter a list of %1$s.', 'woopack' ), $tax->label ),
					'matching'      => true
				), $settings );
			}
			?>
		</table>
		<table class="fl-form-table">
			<?php
			FLBuilder::render_settings_field( 'users', array(
				'type'          => 'suggest',
				'action'        => 'fl_as_users',
				'label'         => __( 'Authors', 'woopack' ),
				'help'          => __( 'Enter a list of authors usernames.', 'woopack' ),
				'matching'      => true
			), $settings);
			?>
		</table>
	</div>
</div>
<div id="fl-builder-settings-section-product-content" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Content Settings', 'woopack' ); ?></h3>
	<table class="fl-form-table">
		<?php
		FLBuilder::render_settings_field( 'product_title', array(
			'type'          => 'select',
			'label'         => __( 'Show Product Title?', 'woopack' ),
			'default'       => 'yes',
			'options'       => array(
				'yes'           => __( 'Yes', 'woopack' ),
				'no'            => __( 'No', 'woopack' ),
			),
			'toggle'    => array(
				'yes'       => array(
					'sections'  => array( 'product_title_fonts', 'product_title_style' ),
				),
			),
		), $settings);

		FLBuilder::render_settings_field( 'product_price', array(
			'type'          => 'select',
			'label'         => __( 'Show Price?', 'woopack' ),
			'default'       => 'yes',
			'options'       => array(
				'yes'           => __( 'Yes', 'woopack' ),
				'no'            => __( 'No', 'woopack' ),
			),
			'toggle'        => array(
				'yes'           => array(
					'sections'      => array( 'product_price_align', 'regular_price_fonts', 'product_price_fonts', 'sale_price_fonts' ),
				),
			),
		), $settings);

		FLBuilder::render_settings_field(
			'product_rating',
			array(
				'type'    => 'select',
				'label'   => __( 'Show Rating?', 'woopack' ),
				'default' => 'yes',
				'options' => array(
					'yes' => __( 'Yes', 'woopack' ),
					'no'  => __( 'No', 'woopack' ),
				),
				'toggle'  => array(
					'yes' => array(
						'sections' => array( 'product_rating_style' ),
						'fields'   => array( 'product_rating_count', 'product_rating_text' ),
					),
				),
			),
			$settings
		);

		FLBuilder::render_settings_field(
			'product_rating_count',
			array(
				'type'    => 'select',
				'label'   => __( 'Show Rating Count?', 'woopack' ),
				'default' => 'no',
				'options' => array(
					'yes' => __( 'Yes', 'woopack' ),
					'no'  => __( 'No', 'woopack' ),
				),
				'toggle'  => array(
					'yes' => array(
						'fields'   => array( 'product_rating_text', 'product_rating_count_color' ),
						'sections' => array( 'rating_count_taxonomy' ),
					),
				),
			),
			$settings
		);

		FLBuilder::render_settings_field(
			'product_rating_text',
			array(
				'type'    => 'text',
				'label'   => __( 'Rating Count Text', 'woopack' ),
				'default' => __( 'customer review', 'woopack' ),
				'connections' => array( 'string', 'html' ),
			),
			$settings
		);

		FLBuilder::render_settings_field(
			'product_short_description',
			array(
				'type'    => 'select',
				'label'   => __( 'Show Short Description?', 'woopack' ),
				'default' => 'no',
				'options' => array(
					'yes' => __( 'Yes', 'woopack' ),
					'no'  => __( 'No', 'woopack' ),
				),
				'toggle'  => array(
					'yes' => array(
						'sections' => array( 'short_description' ),
					),
				),
			),
			$settings
		);
		?>
	</table>
</div>
<div id="fl-builder-settings-section-product-image" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Product Image', 'woopack' ); ?></h3>
	<table class="fl-form-table">
		<?php
		FLBuilder::render_settings_field( 'show_image', array(
			'type'              => 'select',
			'label'             => __( 'Show Image?', 'woopack' ),
			'default'           => 'yes',
			'options'           => array(
				'yes'               => __( 'Yes', 'woopack' ),
				'no'                => __( 'No', 'woopack' ),
			),
			'toggle'            => array(
				'yes'               => array(
					'sections'          => array( 'image_style' ),
					'fields'            => array( 'image_size', 'inside_slider' ),
				),
			),
		), $settings);
		FLBuilder::render_settings_field( 'image_size', array(
			'type'              => 'photo-sizes',
			'label'             => __( 'Size', 'woopack' ),
			'default'           => 'medium',
		), $settings);
		FLBuilder::render_settings_field( 'image_slider', array(
			'type'              => 'select',
			'label'             => __( 'Enable Image Slider?', 'woopack' ),
			'help'				=> __( 'This will enable product images slider on hover.', 'woopack' ),
			'default'           => 'no',
			'options'           => array(
				'yes'               => __( 'Yes', 'woopack' ),
				'no'                => __( 'No', 'woopack' ),
			),
			'preview'			=> array(
				'type'				=> 'none'
			),
		), $settings);
		?>
	</table>
</div>
<div id="fl-builder-settings-section-sale-badge" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Badge', 'woopack' ); ?></h3>
	<table class="fl-form-table">
		<?php
		FLBuilder::render_settings_field( 'show_sale_badge', array(
			'type'              => 'select',
			'label'             => __( 'Show Sale Badge?', 'woopack' ),
			'default'           => 'yes',
			'options'           => array(
				'yes'               => __( 'Yes', 'woopack' ),
				'no'                => __( 'No', 'woopack' ),
			),
			'toggle'            => array(
				'yes'               => array(
					'sections'          => array( 'sale_badge_style', 'sale_badge_fonts' ),
				),
			),
		), $settings);
		?>
	</table>
</div>
<div id="fl-builder-settings-section-out-of-stock" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Out of Stock', 'woopack' ); ?></h3>
	<table class="fl-form-table">
		<?php
		FLBuilder::render_settings_field( 'out_of_stock_text', array(
			'type'              => 'text',
			'label'             => __( 'Custom Text', 'woopack' ),
			'default'           => __( 'Out of Stock', 'woopack' ),
		), $settings);
		?>
	</table>
</div>
<div id="fl-builder-settings-section-product-button" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Button', 'woopack' ); ?></h3>
	<table class="fl-form-table">
		<?php
		FLBuilder::render_settings_field( 'button_type', array(
			'type'              => 'select',
			'label'             => __( 'Button Type', 'woopack' ),
			'default'           => 'cart',
			'options'           => array(
				'cart'              => __( 'Add to Cart', 'woopack' ),
				'custom'			=> __( 'Custom', 'woopack' ),
				'none'              => __( 'None', 'woopack' ),
			),
			'toggle'            => array(
				'cart'				=> array(
					'tabs'				=> array( 'button' ),
					'fields'			=> array( 'qty_input' )
				),
				'custom'			=> array(
					'tabs'				=> array( 'button' ),
					'fields'			=> array( 'button_text', 'button_target' )
				),
			),
		), $settings);

		FLBuilder::render_settings_field( 'button_text', array(
			'type'				=> 'text',
			'label'				=> __( 'Custom Button Text', 'woopack' ),
			'default'			=> __( 'View More', 'woopack' ),
		), $settings);

		FLBuilder::render_settings_field( 'button_target', array(
			'type'				=> 'select',
			'label'				=> __( 'Target', 'woopack' ),
			'default'			=> '_self',
			'options'			=> array(
				'_self'				=> __( 'Same Window', 'woopack' ),
				'_blank'			=> __( 'New Window', 'woopack' )
			)
		), $settings);

		FLBuilder::render_settings_field( 'qty_input', array(
			'type'				=> 'select',
			'label'				=> __( 'Show Quantity Input', 'woopack' ),
			'default'			=> 'no',
			'options'			=> array(
				'no'				=> __( 'No', 'woopack' ),
				'before_button'		=> __( 'Before Button', 'woopack' ),
				'after_button'		=> __( 'After Button', 'woopack' ),
				'above_button'		=> __( 'Above Button', 'woopack' )
			)
		), $settings);

		FLBuilder::render_settings_field( 'variation_fields', array(
			'type'				=> 'select',
			'label'				=> __( 'Enable Variation Fields', 'woopack' ),
			'default'			=> 'no',
			'options'			=> array(
				'yes'				=> __( 'Yes', 'woopack' ),
				'no'				=> __( 'No', 'woopack' ),
			),
			'help'				=> __( 'This will display the product variation fields right in the product grid.', 'woopack' )
		), $settings);
		?>
	</table>
</div>
<div id="fl-builder-settings-section-product-taxonomy" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Product Meta', 'woopack' ); ?></h3>
	<table class="fl-form-table">
		<?php
		FLBuilder::render_settings_field( 'show_taxonomy', array(
			'type'          => 'select',
			'label' 		=> __( 'Show Taxonomy?', 'woopack' ),
			'default' 		=> 'yes',
			'options' 		=> array(
				'yes'           => __( 'Yes', 'woopack' ),
				'no' 			=> __( 'No', 'woopack' ),
			),
			'toggle'	=> array(
				'yes'	=> array(
					'sections'	=> array( 'meta_style' ),
					'fields'	=> array( 'select_taxonomy', 'show_taxonomy_custom_text' )
				)
			)
		), $settings);
		FLBuilder::render_settings_field( 'select_taxonomy', array(
			'type'          => 'select',
			'label' 		=> __( 'Select Taxonomy', 'woopack' ),
			'options' 		=> WooPack_Helper::get_taxonomies_list()
		), $settings);
		FLBuilder::render_settings_field( 'show_taxonomy_custom_text', array(
			'type'          => 'select',
			'label' 		=> __( 'Use Custom Taxonomy Label?', 'woopack' ),
			'default' 		=> 'no',
			'options' 		=> array(
				'yes'           => __( 'Yes', 'woopack' ),
				'no' 			=> __( 'No', 'woopack' ),
			),
			'toggle'	=> array(
				'yes'	=> array(
					'fields'	=> array( 'taxonomy_custom_text' )
				)
			)
		), $settings);
		FLBuilder::render_settings_field( 'taxonomy_custom_text', array(
			'type'              => 'text',
			'label'             => __( 'Custom Label', 'woopack' ),
			'default'			=> '',
			'connections'       => array( 'string' ),
		), $settings);
		?>
	</table>
</div>
<div id="fl-builder-settings-section-quick-view" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Quick View', 'woopack' ); ?></h3>
	<table class="fl-form-table">
		<?php
		FLBuilder::render_settings_field( 'show_quick_view', array(
			'type'              => 'select',
			'label' 		=> __( 'Show Quick View?', 'woopack' ),
			'default' 		=> 'yes',
			'options' 		=> array(
				'yes'           => __( 'Yes', 'woopack' ),
				'no' 			=> __( 'No', 'woopack' ),
			),
			'toggle'    => array(
				'yes'       => array(
					'sections'  => array( 'quick_view_style', 'quick_view_fonts' ),
					'fields'	=> array( 'quick_view_custom_text' )
				),
			),
		), $settings);
		FLBuilder::render_settings_field( 'quick_view_custom_text', array(
			'type'              => 'text',
			'label'             => __( 'Custom Text', 'woopack' ),
			'default'			=> __( 'Quick View', 'woopack' ),
			'connections'       => array( 'string' ),
		), $settings);
		FLBuilder::render_settings_field( 'quick_view_type', array(
			'type'              => 'select',
			'label' 			=> __( 'Type', 'woopack' ),
			'default' 			=> 'image_hover',
			'options' 			=> array(
				'image_hover'       => __( 'Hover on Image', 'woopack' ),
				'overlay' 			=> __( 'Overlay', 'woopack' ),
			),
		), $settings);
		FLBuilder::render_settings_field( 'quick_view_template', array(
			'type'		=> 'select',
			'label'		=> __( 'Template', 'woopack' ),
			'options'	=> array(
				''			=> __( 'Default', 'woopack' ),
			),
		), $settings );
		?>
	</table>
</div>

<?php
do_action( 'woopack_loop_settings_after_form', $settings ); // e.g Add custom FLBuilder::render_settings_field()
