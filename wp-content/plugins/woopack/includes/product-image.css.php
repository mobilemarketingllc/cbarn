<?php if ( ! isset( $settings->layout ) || 'custom' !== $settings->layout ) : ?>

.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-image,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-image {
    position: relative;
	width: 100%;
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-image a,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-image a {
	display: block;
	cursor: pointer;
	height: 100%;
	width: 100%;
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-image a img,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-image a img {
    margin: auto;
    width: auto;
    border: none;
}

<?php endif; ?>