;(function($) {

	WooPackModule = {
		_isQuickViewForm: false,

		_init: function() {
			FLBuilder.addHook( 'settings-form-init', this._initSettingsForms );
			$(FLBuilder._contentClass).on( 'fl-builder.layout-rendered', this._initCustomQty );
		},

		_initSettingsForms: function() {
			if ( $('form[data-form-id="page-data-post-woopack_quick_view_button"]:visible').length > 0 ) {
				this._isQuickViewForm = true;

				var form = $('form[data-form-id="page-data-post-woopack_quick_view_button"]:visible');

				form.find( '#fl-field-icon input[type="hidden"]' ).on('change', function() {
					if ( $(this).next().hasClass('cloned-input') ) {
						$(this).next().remove();
					} else {
						$('<input type="text" name="icon" class="cloned-input" value="' + $(this).val() + '" />').insertAfter( $(this) );
					}
				});
			}
		},

		_initCustomQty: function() {
			// Custom Quantity input.
			if ( $('.woopack-products .woopack-qty-custom').length > 0 ) {
				var minus = '<span class="qty-minus"></span>';
				var plus = '<span class="qty-plus"></span>';
				$('.woopack-products .woopack-qty-custom input.qty').each(function() {
					if ( ! $(this).parent().hasClass('woopack-qty-input') ) {
						$(this).parent().addClass('woopack-qty-input');
					}

					$(minus).insertBefore( $(this) );
					$(plus).insertAfter( $(this) );

					var qty = $(this);
					$(this).parent().find( '.qty-minus' ).on('click', function() {
						qty[0].stepDown();
						qty.trigger('change');
					});
					$(this).parent().find( '.qty-plus' ).on('click', function() {
						qty[0].stepUp();
						qty.trigger('change');
					});
				});
			}
		}
	};

	$( function() { WooPackModule._init(); } );

})(jQuery);