(function($){

	FLBuilder.registerModuleHelper('offcanvas-cart', {
		init: function() {
			$('.fl-builder-settings select[name="direction"]').on( 'change', this._triggerDirectionChange );
			$('.fl-builder-settings .woopack-offcanvas-cart-preview').off('click').on('click', this._renderPreview);
		},

		_triggerDirectionChange: function() {
			var form = $('.fl-builder-settings');
			var nodeId = form.data('node');
			var direction = form.find('select[name="direction"]').val();

			$('#woopack-cart-' + nodeId)
				.removeClass('direction-left')
				.removeClass('direction-right')
				.addClass('direction-' + direction);
		},

		_renderPreview: function(e) {
			e.stopPropagation();

			var node = $('.fl-builder-settings').data('node');
			
			if ( 'undefined' !== typeof window['woopack_' + node] ) {
				window['woopack_' + node]._togglePreview();
			}
		}
	});

})(jQuery);