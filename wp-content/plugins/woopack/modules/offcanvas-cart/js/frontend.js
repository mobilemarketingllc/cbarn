;(function($) {

	WooPackOffCanvasCart = function(settings) {
		this.id 	= settings.id;
		this.node 	= $('.fl-node-' + this.id);
		this.wrap 	= this.node.find('.woopack-offcanvas-cart');
		this.element = '';
		this.offcanvas = '';
		this.isPreview = false;
		this.isBuilderActive = settings.isBuilderActive;

		this._init();
	};

	WooPackOffCanvasCart.prototype = {
		id: '',
		node: '',
		wrap: '',
		element: '',
		offcanvas: '',
		isPreview: false,
		isBuilderActive: false,

		_init: function() {
			var self = this;

			// if ( $( 'body > #woopack-cart-' + this.id ).length > 0 ) {
			// 	$( 'body > #woopack-cart-' + this.id ).remove();
			// }

			if ( $( 'body > #woopack-cart-' + this.id ).length === 0 ) {
				this.node.find('.woopack-offcanvas-cart-panel').appendTo('body');
			}

			// if ( ! this.isBuilderActive ) {
			// 	this.node.find('.woopack-cart-offcanvas').appendTo('body');
			// }

			this.offcanvas = $('#woopack-cart-' + this.id);
			this.offcanvas.find('.woopack-offcanvas-title').html( this.node.find('.cart-button-wrap').html() );

			this.element = this.node.find('a.woopack-cart-contents');

			this.element.on('click', function(e) {
				e.preventDefault();

				self._toggleOffCanvas();
			});

			
			this._bindEvents();
		},

		_bindEvents: function() {
			var self = this;

			$('body').delegate('#woopack-cart-' + this.id + ' .woopack-offcanvas-close', 'click', $.proxy( this._closeOffCanvas, this ));

			$(document).on('click', function(e) {
				if ( ! self.isPreview ) {
					if ( ! self.wrap.is(e.target) && self.wrap.has(e.target).length === 0 && ! $(e.target).has('.woopack-offcanvas-inner') && e.which ) {
						self._closeOffCanvas();
					}
				}
				if ( $(e.target).is(self.offcanvas.find('.woopack-offcanvas-overlay')) ) {
					self._closeOffCanvas();
				}
			});
			
			$(document).keyup(function(e) {
                if ( 27 == e.which && self.offcanvas ) {
                    self.offcanvas.removeClass('woopack-offcanvas-active');
                }
            });

			$(window).resize( $.proxy( this._resize, this ) );
		},

		_toggleOffCanvas: function() {
			this.offcanvas.toggleClass('woopack-offcanvas-active');

			this._resize();
		},

		_showOffCanvas: function() {
			if ( this.offcanvas ) {
				this.offcanvas.addClass('woopack-offcanvas-active');
				this._resize();
			}
		},

		_closeOffCanvas: function() {
			if ( this.offcanvas ) {
				this.offcanvas.removeClass('woopack-offcanvas-active');
			}
		},

		_resize: function() {
			if ( this.offcanvas && this.offcanvas.length > 0 ) {
				var winHeight = window.innerHeight;
				var offset = 0;

				// if ( $('body').hasClass('admin-bar') ) {
				// 	offset = 32;
				// }

				winHeight = winHeight - offset;

				this.offcanvas.find('.woopack-offcanvas-inner').css({
					'height': winHeight + 'px',
					'top': offset
				});

				headerHeight = this.offcanvas.find('.woopack-offcanvas-header').outerHeight();
				cartTotalHeight = this.offcanvas.find('.woocommerce-mini-cart__total').outerHeight();
				cartButtonsHeight = this.offcanvas.find('.woocommerce-mini-cart__buttons').outerHeight();
				itemsHeight = winHeight - (headerHeight + cartTotalHeight + cartButtonsHeight);

				var style = '<style id="woopack-style-' + this.id + '">';
				style += '#' + this.offcanvas.attr('id') + ' .woocommerce-mini-cart.cart_list {'
				style += 'height: ' + itemsHeight + 'px';
				style += '}';
				style += '</style>';

				if ( $('#woopack-style-' + this.id).length > 0 ) {
					$('#woopack-style-' + this.id).remove();	
				}

				$('head').append(style);
			}
		},

		_togglePreview: function() {
			if ( ! this.isPreview ) {
				this.isPreview = true;
				this._showOffCanvas();
			} else {
				this.isPreview = false;
				this._closeOffCanvas();
			}
		}
	};

})(jQuery);