<?php do_action( 'woopack_single_product_before_button_wrap', $settings, $product ); ?>

<div class="woopack-product-action  woocommerce-product-add-to-cart">
	<a href="<?php echo $settings->button_link; ?>" target="<?php echo $settings->button_link_target; ?>" class="button woopack-product-button woopack-product-button-custom">
		<?php
		if ( $settings->button_text != '' ) {
			echo $settings->button_text;
		} else {
			esc_html_e('Add to cart', 'woopack');
		}
		?>
	</a>
</div>

<?php do_action( 'woopack_single_product_after_button_wrap', $settings, $product ); ?>