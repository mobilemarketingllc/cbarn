;(function($) {

    WooPackGrid = function(settings) {
		this.settings       = settings;
		this.nodeClass      = '.fl-node-' + settings.id;
		this.wrapperClass   = this.nodeClass + ' .woopack-products-grid-wrap ul.products';
		this.postClass      = this.wrapperClass + ' .woopack-product-grid';
        this.perPage        = settings.perPage;
		this.matchHeight	= settings.matchHeight === 'yes' ? true : false;
        this.layoutStyle	= settings.layoutStyle;
        this.masonry        = settings.matchHeight === 'no' ? true : false;
        this.filters        = settings.filters === 'yes' ? true : false;
        this.filterTax      = settings.filterTax;
		this.filterType     = settings.filterType;
		this.isotopeData 	= {};
		this.template 		= settings.template;
		this.cacheData		= {};

		if(this._hasPosts()) {
			this._initInfiniteScroll();
			//this._matchWidth();
			this._gridLayout();
			this._reLayout();
		}

		// Workaround for variation after AJAX filters.
		$(document).on('woopack.grid.rendered', function(e, obj) {
			if ( $( obj.postClass ).hasClass( 'product-type-variable' ) ) {
				$.get( woopack_config.woo_url + 'assets/js/frontend/add-to-cart-variation.min.js' );
			}
		});
	};

    WooPackGrid.prototype = {

		settings        : {},
		nodeClass       : '',
		wrapperClass    : '',
		postClass       : '',
        perPage         : '',
        matchHeight     : false,
        layoutStyle     : 1,
        masonry         : false,
        filters         : false,
        filterTax       : '',
		filterType      : '',
		isotopeData 	: '',
		template		: '',
		cacheData		: {},

		_hasPosts: function()
		{
			return $(this.postClass).length > 0;
		},

        _matchWidth: function()
		{
            var wrap = $(this.wrapperClass);

			wrap.imagesLoaded( $.proxy( function() {
				if ( 3 == this.layoutStyle || 4 == this.layoutStyle ) {
				   var img_width = $( this.postClass + ' .woopack-product-image img').attr('width');
					$( this.postClass + ' .woopack-product-content').css({ 'width': 'calc(100% - ' + img_width+ 'px)' });
			    }
			}, this ) );
        },

        _gridLayout: function()
		{
			var self = this;
			var wrap = $(this.wrapperClass);

            var postFilterData = {
				itemSelector: '.woopack-product-grid',
				percentPosition: true,
				transitionDuration: '0.2s',
				isOriginLeft: ! $('body').hasClass( 'rtl' ),
			};

			if ( ! this.masonry ) {
				postFilterData = $.extend( {}, postFilterData, {
					layoutMode: 'fitRows',
					fitRows: {
						gutter: '.woopack-grid-sizer'
					},
				} );
			}

			if ( this.masonry ) {

				postFilterData = $.extend( {}, postFilterData, {
					masonry: {
						columnWidth: '.woopack-product-grid',
						gutter: '.woopack-grid-sizer'
					},
				} );
			}

			this.isotopeData = postFilterData;

			wrap.imagesLoaded( $.proxy( function() {

				var node = $(this.nodeClass);
				var base = this;

				this._initSlider();

				var postFilters = $(this.nodeClass).find('ul.products').isotope(postFilterData);

                if ( this.filters || this.masonry ) {

                    var filterWrap = $(this.nodeClass).find('.woopack-product-filters');
					var filterToggle = $(this.nodeClass).find('.woopack-product-filters-toggle');
					
					filterToggle.on('click', function() {
						filterWrap.slideToggle(function() {
							if ( $(this).is(':visible') ) {
								$(this).addClass('woopack-product-filters-open');
							}
							if ( ! $(this).is(':visible') ) {
								$(this).removeClass('woopack-product-filters-open');
							}
						});
					});

                    filterWrap.on('click keyup', '.woopack-product-filter', function(e) {
						var shouldFilter = 'click' === e.type || ( 'keyup' === e.type && ( 13 === e.keyCode || 13 === e.which ) )
						if ( ! shouldFilter ) {
							return;
						}
                        if ( 'static' === base.filterType ) {
                            var filterVal = $(this).attr('data-filter');
                            postFilters.isotope({ filter: filterVal });
                        } else {
							$(base.nodeClass).addClass( 'woopack-products-loading' );
                            var term = $(this).data('term');
							base._getPosts(term, postFilterData);
						}

                        filterWrap.find('.woopack-product-filter').removeClass('woopack-filter-active');
						$(this).addClass('woopack-filter-active');
						
						filterToggle.find('span.toggle-text').html($(this).text());
						if (filterWrap.hasClass('woopack-product-filters-open')) {
							filterWrap.slideUp();
						}
						$(base.nodeClass).trigger('grid.filter.change');
					});
					
					if ('dynamic' === base.filterType) {
						$(base.nodeClass).find('.fl-builder-pagination a').off('click').on('click', function (e) {
							e.preventDefault();
							var pageNumber = $(this).attr('href').split('#page-')[1];
							base.currentPage = pageNumber;
							base._getPosts('', postFilterData, pageNumber);
						});
					}

					// Trigger filter by hash parameter in URL.
					if ( '' !== location.hash ) {
						var filterHash = location.hash.split('#')[1];

						filterWrap.find('li[data-term="' + filterHash + '"]').trigger('click');
					}

					// Trigger filter on hash change in URL.
					$(window).on('hashchange', function() {
						if ( '' !== location.hash ) {
							var filterHash = location.hash.split('#')[1];
	
							filterWrap.find('li[data-term="' + filterHash + '"]').trigger('click');
						}
					});
                }

                if( !this.masonry ) {
                    setTimeout( function() {
						base._gridLayoutMatchHeight();
						node.find('ul.products').isotope('layout');
                    }, 1000 );
                }

                if ( this.filters || this.masonry ) {
                    setTimeout( function() {
                        if ( 'static' === base.filterType ) {
                            node.find('.woopack-filter-active').trigger('click');
                        }
						if ( ! base.masonry ) {
                        	base._gridLayoutMatchHeight();
						}
						node.find('ul.products').isotope('layout');
                    }, 1000 );
                }

			}, this ) );
		},

		_initSlider: function() {
			if ( $('body').hasClass('fl-builder-edit') ) {
				return;
			}

			var wrap = $(this.wrapperClass);

			if ( ! this.settings.imagesSlider ) {
				// Workaround for custom layout.
				if ( 'undefined' === typeof wrap.find( '.woopack-has-slider' ).get(0) ) {
					return;
				}
			}
			if ( 'undefined' === typeof jQuery.fn.slick ) {
				return;
			}

			wrap.find('.product').each(function() {
				var product = $(this),
					slider = $(this).find('.woopack-product-images'),
					slides = slider.find('.woopack-product-image-slide');

				if ( slides.length > 1 && slider.find('.slick-slide').length === 0 ) {
					if ( slides.length === 1 ) {
						return;
					}
					var opts = {
						autoplay: true,
						autoplaySpeed: 600,
						pauseOnHover: false,
						arrows: false,
						adaptiveHeight: false,
						centerMode: false,
						cssEase: 'ease',
						rtl: $('body').hasClass('rtl')
					};
					slider.slick( opts );
					setTimeout(function() {
						slider.slick('slickPause');
					}, 200);
				}
			});
			
			wrap.find('.product').on('mouseenter', function(e) {
				e.stopPropagation();
				var product = $(this),
					slider = $(this).find('.woopack-product-images'),
					slides = slider.find('.woopack-product-image-slide');
				slider.show(function() {
					if ( slides.length > 1 ) {
						slider.slick('slickPlay');
					}
				});
			}).on('mouseleave', function(e) {
				e.stopPropagation();
				var product = $(this),
					slider = $(this).find('.woopack-product-images');

				if ( slider.find('.slick-slide').length === 0 ) {
					return;
				}

				slider.slick('slickPause');

				setTimeout(function() {
					slider.slick('slickGoTo', 0);
				}, 100);
			});
		},

		_getPosts: function (term, isotopeData, paged) {
			var processAjax = false,
				filter 		= term,
				paged 		= (!paged || 'undefined' === typeof paged) ? 1 : paged;

			if ('undefined' === typeof term || '' === filter) {
				filter = 'all';
			}

			var cacheData = this._getCacheData(filter);

			if ('undefined' === typeof cacheData) {
				processAjax = true;
			} else {
				var cachedResponse = cacheData.page[paged];
				if ('undefined' === typeof cachedResponse) {
					processAjax = true;
				} else {
					this._renderPosts(cachedResponse, {
						term: term,
						isotopeData: isotopeData,
						page: paged
					});
				}
			}

			if (processAjax) {
				this._getAjaxPosts(term, isotopeData, paged);
			}
		},

        _getAjaxPosts: function(term, isotopeData, paged)
        {
            var taxonomy    = this.filterTax,
				perPage     = this.perPage,
				layout      = this.layoutStyle,
				paged 		= 'undefined' === typeof paged ? false : paged,
                self        = this;

            var wrap        = $(this.wrapperClass),
				gridWrap    = $(this.nodeClass).find('.woopack-products-grid-wrap');
				
			var currentPage = woopack_config.current_page.split('?')[0];

            var data = {
                woopack_action: 'woopack_grid_get_posts',
                node_id: this.settings.id,
				paged: !paged ? woopack_config.page : paged,
				current_page: currentPage,
                settings: this.settings.fields
            };

            if ( '' !== term ) {
                data['term'] = term;
			}
			if ( 'undefined' !== self.settings.orderby || '' !== self.settings.orderby ) {
				data['orderby'] = self.settings.orderby;
			}

			gridWrap.addClass('woopack-is-filtering');

			$.ajax({
				type: 'post',
				url: window.location.href.split('#').shift(),
				data: data,
				success: function (response) {
					self._setCacheData(term, response, paged);
					self._renderPosts(response, {
						term: term,
						isotopeData: isotopeData,
						page: paged
					});
					gridWrap.removeClass('woopack-is-filtering');
				}
			});
		},
		
		_renderPosts: function (response, args)
		{
			var self = this,
				wrap = $(this.wrapperClass),
				gridWrap = $(this.nodeClass).find('.woopack-products-grid-wrap'); //console.log('_renderPosts');

			if ( $(self.nodeClass).hasClass('woopack-products-loading') || ( 'load_more' !== self.settings.pagination && 'scroll' !== self.settings.pagination ) ) {
				wrap.isotope('remove', $(this.postClass));
				$(self.nodeClass).removeClass('woopack-products-loading');
			}

			wrap.isotope('insert', $(response.data), $.proxy(this._isotopeCallback, this));

			wrap.find('.woopack-grid-sizer').remove();
			wrap.append('<li class="woopack-grid-sizer"></li>');

			wrap.imagesLoaded(function () {
				setTimeout(function () {
					if (!self.masonry) {
						self._gridLayoutMatchHeight();
					}
					wrap.isotope('layout');
					setTimeout(function() {
						self._initSlider();
					}, 500);
				}, 200);

				$(document).trigger( 'woopack.grid.rendered', [self] );
			});

			if (response.pagination) {
				$(self.nodeClass).find('.fl-builder-pagination').remove();
				$(self.nodeClass).find('.fl-module-content').append(response.pagination);
				$(self.nodeClass).find('.woopack-ajax-pagination a').off('click').on('click', function (e) {
					e.preventDefault();
					var pageNumber = $(this).attr('href').split('#page-')[1];
					self._getPosts(args.term, args.isotopeData, pageNumber);
				});
			} else {
				$(self.nodeClass).find('.fl-builder-pagination').remove();
			}

			var offsetTop = gridWrap.offset().top - 200;
			$('html, body').stop().animate({
				scrollTop: offsetTop
			}, 300);
		},

		_isotopeCallback: function() {
			var self = this,
				wrap = $(this.wrapperClass);

			if ( ! this.masonry ) {
				wrap.imagesLoaded(function () {
					setTimeout(function () {
						self._gridLayoutMatchHeight();
					}, 150);
				});
			}
			$( this.wrapperClass ).isotope( 'layout' );
			$( this.nodeClass ).removeClass('woopack-products-loading');
		},

		_setCacheData: function (filter, response, paged)
		{
			if ('undefined' === typeof filter || '' === filter) {
				filter = 'all';
			}
			if ('undefined' === typeof paged || !paged) {
				paged = 1;
			}

			if ('undefined' === typeof this.cacheData.ajaxCache) {
				this.cacheData.ajaxCache = {};
			}
			if ('undefined' === typeof this.cacheData.ajaxCache[filter]) {
				this.cacheData.ajaxCache[filter] = {};
			}
			if ('undefined' === typeof this.cacheData.ajaxCache[filter].page) {
				this.cacheData.ajaxCache[filter].page = {};
			}

			return this.cacheData.ajaxCache[filter].page[paged] = response;
		},

		_getCacheData: function (filter)
		{
			var cacheData = this.cacheData;

			if ('undefined' === typeof cacheData.ajaxCache) {
				cacheData.ajaxCache = {};
			}
			//console.log(cacheData);

			return cacheData.ajaxCache[filter];
		},

		_gridLayoutMatchHeight: function()
		{
			var highestBox = 0;
			var postElements = $(this.postClass + ':visible');
			var columns = this.settings.columns.desktop;

			if (! this.matchHeight || 1 === columns) {
				return;
			}

			if ( 'style-9' === this.style ) {
				return;
			}

			if ( this.masonry ) {
				return;
			}

			if (window.innerWidth <= 980) {
				columns = this.settings.columns.medium;
			}
			if (window.innerWidth <= 767) {
				columns = this.settings.columns.responsive;
			}

			if ( 1 === columns ) {
				return;
			}

			postElements.css('height', 'auto');

			var rows = Math.round(postElements.length / columns);

			if ( postElements.length % columns > 0 ) {
				rows = rows + 1;
			}

			// range.
			var j = 1,
				k = columns;

			for( var i = 0; i < rows; i++ ) {
				// select number of posts in the current row.
				var postsInRow = $(this.postClass + ':visible:nth-child(n+' + j + '):nth-child(-n+' + k + ')');

				// get height of the larger post element within the current row.
				postsInRow.css('height', '').each(function () {
					if ($(this).height() > highestBox) {
						highestBox = $(this).height();
					}
				});
				// apply the height to all posts in the current row.
				postsInRow.height(highestBox);

				// increment range.
				j = k + 1;
				k = k + columns;
				if ( k > postElements.length ) {
					k = postElements.length;
				}
				highestBox = 0;
			}
		},

		_reLayout: function()
		{
			var self = this;
			var wrap = $(this.wrapperClass);

			// Search and Filter plugin fix.
			$(document).on('sf:ajaxfinish', '.searchandfilter', function(){
				self._gridLayout();
			});

			// FacetWP fix.
			$(document).on('facetwp-loaded', function() {
				wrap.imagesLoaded(function() {
					if ( ! self.masonry ) {
						self._gridLayoutMatchHeight();
					}
					setTimeout(function() {
						self._initSlider();
						if ( wrap.data( 'isotope' ) ) {
							wrap.isotope('destroy');
						}
						self._gridLayout();
					}, 500);
				});
			});

			// PowerPack Advanced Tabs fix.
			$(document).on('pp-tabs-switched', function(e, content) {
				if ( content.find(this.nodeClass).length === 0 ) {
					return;
				}
				if ( ! self.masonry ) {
					self._gridLayoutMatchHeight();
				}
				if ( self.filters || self.masonry ) {
					if ( content.find('ul.products').data( 'isotope' ) ) {
						content.find('ul.products').isotope('layout');
					} else {
						self._gridLayout();
					}
				}
				setTimeout(function() {
					self._initSlider();
				}, 500);
			});
		},

		_initInfiniteScroll: function()
		{
			if (this.settings.pagination == 'scroll' && typeof FLBuilder === 'undefined') {
				this._infiniteScroll();
			}
		},

		_infiniteScroll: function(settings)
		{
			$(this.wrapperClass).infinitescroll({
				navSelector     : this.nodeClass + ' .fl-builder-pagination',
				nextSelector    : this.nodeClass + ' .fl-builder-pagination a.next',
				itemSelector    : this.postClass,
				prefill         : true,
				bufferPx        : 200,
				animate			: false,
				loading         : {
					msgText         : 'Loading',
					finishedMsg     : '',
					img             : FLBuilderLayoutConfig.paths.pluginUrl + 'img/ajax-loader-grey.gif',
					speed           : 1
				}
			}, $.proxy(this._infiniteScrollComplete, this));

			setTimeout(function(){
				$(window).trigger('resize');
			}, 100);
		},

		_infiniteScrollComplete: function(elements)
		{
			var self = this;
			var wrap = $(this.wrapperClass);

			elements = $(elements);

			if (!this.masonry) {
				wrap.isotope('insert', elements, $.proxy(this._gridLayoutMatchHeight, this));
				wrap.imagesLoaded($.proxy(function () {
					setTimeout(function () {
						self._gridLayoutMatchHeight();
					}, 150);
				}, this));
			} else {
				wrap.isotope('insert', elements);
			}
			
			elements.css('visibility', 'visible');
			wrap.find('.woopack-grid-sizer').remove();
			wrap.append('<li class="woopack-grid-sizer"></li>');

			wrap.imagesLoaded($.proxy(function () {
				setTimeout(function () {
					if (!this.masonry) {
						self._gridLayoutMatchHeight();
					}
					wrap.isotope('layout');
					setTimeout(function() {
						self._initSlider();
					}, 500);
				}, 200);

				$(document).trigger( 'woopack.grid.rendered', [self] );
			}, this));
		}
	};

})(jQuery);
