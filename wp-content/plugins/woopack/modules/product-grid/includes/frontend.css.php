<?php
// Common
include WOOPACK_DIR . 'includes/product-common.css.php';
// Image
include WOOPACK_DIR . 'includes/product-image.css.php';
// Sale badge
include WOOPACK_DIR . 'includes/product-badge.css.php';
// Quick View
include WOOPACK_DIR . 'includes/product-quick-view.css.php';
// Content, price.
include WOOPACK_DIR . 'includes/product-content.css.php';
// Actions, button
include WOOPACK_DIR . 'includes/product-action.css.php';

// ******************* Typography *******************
// Product Filters Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'filter_typography',
	'selector' 		=> ".fl-node-$id .woopack-product-filters .woopack-product-filter",
) );
// Pagination Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'pagination_typography',
	'selector' 		=> ".fl-node-$id .fl-builder-pagination li a.page-numbers,
						.fl-node-$id .fl-builder-pagination li span.page-numbers",
) );

// ******************* Border *******************
// Pagination Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'pagination_border',
	'selector' 		=> ".fl-node-$id .fl-builder-pagination li a.page-numbers,
						.fl-node-$id .fl-builder-pagination li span.page-numbers",
) );

// ******************* Padding *******************
// Pagination Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'pagination_padding',
	'selector' 		=> ".fl-node-$id .fl-builder-pagination li a.page-numbers,
						.fl-node-$id .fl-builder-pagination li span.page-numbers",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'pagination_padding_top',
		'padding-right' 	=> 'pagination_padding_right',
		'padding-bottom' 	=> 'pagination_padding_bottom',
		'padding-left' 		=> 'pagination_padding_left',
	),
) );

// ******************* Margin *******************
// Pagination margin
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'pagination_margin',
	'selector' 		=> ".fl-node-$id .fl-builder-pagination li a.page-numbers,
						.fl-node-$id .fl-builder-pagination li span.page-numbers",
	'unit'			=> 'px',
	'props'			=> array(
		'margin-top' 		=> 'pagination_margin_top',
		'margin-right' 		=> 'pagination_margin_right',
		'margin-bottom'		=> 'pagination_margin_bottom',
		'margin-left' 		=> 'pagination_margin_left',
	),
) );

?>
<?php
	$columns_desktop = empty( $settings->product_columns ) ? 3 : intval( $settings->product_columns );
	$columns_medium = empty( $settings->product_columns_medium ) ? $columns_desktop : intval( $settings->product_columns_medium );
	$columns_responsive = empty( $settings->product_columns_responsive ) ? $columns_medium : intval( $settings->product_columns_responsive );
	$space_desktop  = '' === $settings->product_spacing ? 2 : $settings->product_spacing;
	$space_medium  = '' === $settings->product_spacing_medium ? $space_desktop : $settings->product_spacing_medium;
	$space_responsive  = '' === $settings->product_spacing_responsive ? $space_medium : $settings->product_spacing_responsive;
	$width_desktop  = ( 100 - ( ( $columns_desktop - 1 ) * $space_desktop ) ) / $columns_desktop;
	$width_medium  = ( 100 - ( ( $columns_medium - 1 ) * $space_medium ) ) / $columns_medium;
	$width_responsive  = ( 100 - ( ( $columns_responsive - 1 ) * $space_responsive ) ) / $columns_responsive;
?>
.fl-node-<?php echo $id; ?> .woocommerce-result-count {
	display: none;
}

.fl-node-<?php echo $id; ?> .woopack-grid-sizer {
	<?php WooPack_Helper::print_css( 'width', $space_desktop, '%' ); ?>
	margin: 0;
}

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products {
	margin: 0 !important;
}
.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products li.product {
	<?php WooPack_Helper::print_css( 'width', $width_desktop, '% !important' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $space_desktop, '% !important' ); ?>
}
<?php if ( 'yes' == $settings->match_height ) { ?>
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products li.product,
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns; ?>n) {
		margin-right: 0 !important;
	}
	.rtl .fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products li.product,
	.rtl .fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns; ?>n) {
		margin-left: 0 !important;
	}
<?php } else { ?>
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products li.product {
		margin-right: 0 !important;
	}
	.rtl .fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products.woopack-products li.product {
		margin-left: 0 !important;
	}
<?php } ?>

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap.woopack-filters-enabled ul.products.woopack-products li.product {
	margin-right: 0 !important;
}
.rtl .fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap.woopack-filters-enabled ul.products.woopack-products li.product {
	margin-left: 0 !important;
}

.fl-node-<?php echo $id; ?> .fl-builder-pagination {
	width: 100%;
}
.fl-node-<?php echo $id; ?> .fl-builder-pagination ul.page-numbers {
	<?php if ( $default_align && 'default' == $settings->pagination_align ) { ?>
		<?php WooPack_Helper::print_css( 'text-align', $default_align ); ?>
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'text-align', $settings->pagination_align ); ?>
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .fl-builder-pagination li a.page-numbers,
.fl-node-<?php echo $id; ?> .fl-builder-pagination li span.page-numbers {
	<?php WooPack_Helper::print_css( 'background-color', $settings->pagination_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->pagination_color ); ?>
}
.fl-node-<?php echo $id; ?> .fl-builder-pagination li a.page-numbers:hover,
.fl-node-<?php echo $id; ?> .fl-builder-pagination li span.current,
.fl-node-<?php echo $id; ?> .fl-builder-pagination li span[area-current] {
	<?php WooPack_Helper::print_css( 'background-color', $settings->pagination_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->pagination_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->pagination_border_color_hover ); ?>
}

<?php if ( 3 == $settings->product_layout || 4 == $settings->product_layout ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-wrapper {
		display: flex;
		justify-content: space-evenly;
		flex-direction: row;
	}
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products .woopack-product-wrapper .woopack-product-image {
		width: 30%
	}
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products .woopack-product-content,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-content {
		width: 70%;
	}
<?php } ?>

.fl-node-<?php echo $id; ?> .woopack-product-filters-wrap {
	text-align: <?php echo $settings->filter_alignment; ?>;
}
.fl-node-<?php echo $id; ?> .woopack-product-filters {
	<?php if ( 'center' === $settings->filter_alignment ) { ?>
	justify-content: center;
	<?php } ?>
	<?php if ( 'left' === $settings->filter_alignment ) { ?>
	justify-content: flex-start;
	<?php } ?>
	<?php if ( 'right' === $settings->filter_alignment ) { ?>
	justify-content: flex-end;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-filters-toggle {
	<?php WooPack_Helper::print_css( 'color', $settings->filter_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter {
	<?php WooPack_Helper::print_css( 'color', $settings->filter_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->filter_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'border-style', $settings->filter_border, '', 'default' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-top-style', $settings->filter_border, '', 'top' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-bottom-style', $settings->filter_border, '', 'bottom' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-width', $settings->filter_border_width, 'px', 'default' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-top-width', $settings->filter_border_width, 'px', 'top' == $settings->filter_border_pos ); ?>
	<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->filter_border_width, 'px', 'bottom' == $settings->filter_border_pos ); ?>
	border-color: transparent;
	<?php if ( 'all' == $settings->filter_border_el ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->filter_border_color ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->filter_padding_v, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->filter_padding_v, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->filter_padding_h, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->filter_padding_h, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-right', $settings->filter_margin_h, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->filter_margin_v, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->filter_radius, 'px' ); ?>
}
.rtl .fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter {
	<?php WooPack_Helper::print_css( 'margin-left', $settings->filter_margin_h, 'px' ); ?>
	margin-right: 0;
}
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter:hover,
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter:focus,
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter.woopack-filter-active {
	<?php WooPack_Helper::print_css( 'color', $settings->filter_hover_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->filter_bg_hover_color ); ?>
	<?php if ( 'all' == $settings->filter_border_el ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->filter_border_hover_color ); ?>
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter.woopack-filter-active {
	<?php if ( 'active' == $settings->filter_border_el ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->filter_border_color ); ?>
	<?php } ?>
}

<?php
	$breakpoints = apply_filters( 'woopack_product_grid_breakpoints', array(
		'medium' => $global_settings->medium_breakpoint,
		'responsive' => $global_settings->responsive_breakpoint
	), $settings );
?>
@media only screen and (min-width: <?php echo $breakpoints['responsive'] - 1; ?>px ) {
	<?php if ( isset( $settings->filter_position ) && 'top' !== $settings->filter_position && 'yes' === $settings->enable_filter ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-grid-wrap {
			display: flex;
			flex-direction: <?php echo 'right' !== $settings->filter_position ? 'row' : 'row-reverse'; ?>;
		}
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap {
			width: 100%;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-filters {
			flex-direction: column;
		}
		.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter {
			<?php if ( 'right' === $settings->filter_position ) { ?>
			margin-left: <?php echo $settings->filter_margin_h; ?>px;
			margin-right: 0;
			<?php } ?>
		}
	<?php } ?> 
}

@media only screen and (max-width: <?php echo $breakpoints['medium']; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product.woopack-product-grid {
		width: <?php echo $width_medium; ?>% !important;
		<?php WooPack_Helper::print_css( 'margin-bottom', $space_medium, '% !important' ); ?>
		float: left !important;
	}
	.fl-node-<?php echo $id; ?> .woopack-grid-sizer {
		<?php WooPack_Helper::print_css( 'width', $space_medium, '%' ); ?>
	}

	<?php if ( $settings->match_height == 'yes' ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns; ?>n) {
			margin-right: 0% !important;
		}
		.rtl .fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns; ?>n) {
			margin-left: 0% !important;
		}
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_medium; ?>n) {
			margin-right: 0 !important;
		}
		.rtl .fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_medium; ?>n) {
			margin-left: 0 !important;
		}
	<?php } ?>
}

@media only screen and (max-width: <?php echo $breakpoints['responsive']; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product.woopack-product-grid {
		width: <?php echo $width_responsive; ?>% !important;
		<?php WooPack_Helper::print_css( 'margin-bottom', $space_responsive, '% !important' ); ?>
		float: left !important;
	}
	.fl-node-<?php echo $id; ?> .woopack-grid-sizer {
		<?php WooPack_Helper::print_css( 'width', $space_responsive, '%' ); ?>
	}

	<?php if ( $settings->match_height == 'yes' ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_medium; ?>n) {
			margin-right: 0% !important;
		}
		.rtl .fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_medium; ?>n) {
			margin-left: 0% !important;
		}
		.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_responsive; ?>n) {
			margin-right: 0 !important;
		}
		.rtl .fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products.woopack-products li.product:nth-of-type(<?php echo $settings->product_columns_responsive; ?>n) {
			margin-left: 0 !important;
		}
	<?php } ?>

	<?php if ( 3 == $settings->product_layout || 4 == $settings->product_layout ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-wrapper {
		display: flex;
		justify-content: space-evenly;
		flex-direction: column;
		align-items: center;
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-content,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-content {
		width: 100%;
		text-align: center;
	}
	.fl-node-<?php echo $id; ?> .woopack-products-grid-wrap ul.products .woopack-product-wrapper .woopack-product-image {
		width: 100%;
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating {
		margin-left: auto !important;
		margin-right: auto !important;
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products .woopack-product-action,
	.fl-node-<?php echo $id; ?> .woocommerce div.products .woopack-product-action {
		text-align: center;
	}
	<?php } ?>

	.fl-node-<?php echo $id; ?> .woopack-product-filters-wrap {
		text-align: left;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-filters-toggle {
		display: block;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-filters {
		display: none;
	}
	.fl-node-<?php echo $id; ?> .woopack-product-filters .woopack-product-filter {
		display: block;
		float: none;
		margin: 0 !important;
		text-align: left;
	}
}
