<?php
// ******************* Border *******************
// Cart Table Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'table_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce .woocommerce-cart-form table.shop_table.cart",
) );
// Cart Total Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'cart_total_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce .cart_totals table.shop_table",
) );
// Standard Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'standard_button_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce button.button,
						.fl-node-$id .woocommerce input.button",
) );
// Border - Hover Settings
if ( ! empty( $settings->standard_button_border_color_h ) && is_array( $settings->standard_button_border_group ) ) {
	$settings->standard_button_border_group['color'] = $settings->standard_button_border_color_h;
}

FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'standard_button_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce button.button:hover,
						.fl-node-$id .woocommerce input.button:hover",
) );
// Checkout Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'checkout_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce .wc-proceed-to-checkout a.button.alt",
) );
// Border - Hover Settings
if ( ! empty( $settings->checkout_border_color_h ) && is_array( $settings->checkout_border_group ) ) {
	$settings->checkout_border_group['color'] = $settings->checkout_border_color_h;
}

FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'checkout_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce .wc-proceed-to-checkout a.button.alt:hover",
) );

// ******************* Padding *******************
// Checkout Button Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'checkout_padding',
	'selector' 		=> ".fl-node-$id .woocommerce .wc-proceed-to-checkout a.button.alt",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'checkout_padding_top',
		'padding-right' 	=> 'checkout_padding_right',
		'padding-bottom' 	=> 'checkout_padding_bottom',
		'padding-left' 		=> 'checkout_padding_left',
	),
) );

// ******************* Typography *******************
// Default Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'default_typography',
	'selector' 		=> ".fl-node-$id .woocommerce,
						.fl-node-$id .woocommerce .cart_totals table.shop_table tbody th,
						.fl-node-$id .woocommerce .cart_totals table.shop_table tbody td",
) );
// Table Header Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'table_header_typography',
	'selector' 		=> ".fl-node-$id .woocommerce .woocommerce-cart-form table.shop_table.cart thead th",
) );

// Cart Totals Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'cart_totals_title_typography',
	'selector' 		=> ".fl-node-$id .woocommerce .cart_totals h2",
) );

// Standard Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'standard_button_typography',
	'selector' 		=> ".fl-node-$id .woocommerce button.button,
						.fl-node-$id .woocommerce input.button",
) );

// Checkout Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'checkout_button_typography',
	'selector' 		=> ".fl-node-$id .woocommerce .wc-proceed-to-checkout a.button.alt",
) );
?>

.fl-node-<?php echo $id; ?> .woocommerce table.cart th.product-quantity{
	text-align: left;
}
.fl-node-<?php echo $id; ?> .woocommerce,
.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table tbody th,
.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table tbody td {
	<?php WooPack_Helper::print_css( 'color', $settings->default_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart {
	<?php WooPack_Helper::print_css( 'background-color', $settings->table_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->table_padding_t_b, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_padding_t_b, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->table_padding_r_l, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->table_padding_r_l, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart thead th {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding, 'px' ); ?>

	<?php if ( '' != $settings->table_header_border_width ) { ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-style', 'solid' ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->table_header_border_color, ' !important' ); ?>

	<?php WooPack_Helper::print_css( 'color', $settings->table_header_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce tr.woocommerce-cart-form__cart-item.cart_item:nth-child(even) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_even_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce tr.woocommerce-cart-form__cart-item.cart_item:nth-child(odd) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_odd_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart tbody tr:first-child td {
	border-top-width: 0;
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart td {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding, 'px' ); ?>

	<?php if ( '' != $settings->cart_item_border_width ) { ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width, 'px' ); ?>
		border-top-style: solid;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->cart_item_border_color, ' !important' ); ?>
}

.fl-node-<?php echo $id; ?> .woocommerce table.cart .product-thumbnail img {
	<?php WooPack_Helper::print_css( 'width', $settings->image_width, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart td.product-remove a {
	<?php WooPack_Helper::print_css( 'color', $settings->product_remove_color, ' !important' ); ?>
	background: unset;
	<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom, 'px', 'custom' == $settings->product_remove_font_size ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart td.product-remove a:hover {
	<?php WooPack_Helper::print_css( 'color', $settings->product_remove_color_hover, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table.cart tbody td.product-name a {
	<?php WooPack_Helper::print_css( 'color', $settings->product_name_color ); ?>
	text-decoration: none;
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table.cart tbody td.product-name a:hover {
	<?php WooPack_Helper::print_css( 'color', $settings->product_name_color_hover ); ?>
}
<?php if ( 'yes' == $settings->show_coupon ) { ?>
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.cart td.actions .coupon .input-text {
		<?php WooPack_Helper::print_css( 'color', $settings->coupon_text_color ); ?>
		margin: 0 4px 0 0;
	}
	.fl-node-<?php echo $id; ?> .woocommerce button.button,
	.fl-node-<?php echo $id; ?> .woocommerce input.button {
		<?php WooPack_Helper::print_css( 'background-color', $settings->standard_button_bg_color ); ?>
		<?php WooPack_Helper::print_css( 'color', $settings->standard_button_color, ' !important' ); ?>
		line-height: 1;
	}

	.fl-node-<?php echo $id; ?> .woocommerce button.button:hover,
	.fl-node-<?php echo $id; ?> .woocommerce input.button:hover {
		<?php WooPack_Helper::print_css( 'background-color', $settings->standard_button_bg_color_hover ); ?>
		<?php WooPack_Helper::print_css( 'color', $settings->standard_button_color_hover, ' !important' ); ?>
	}
<?php } // End if(). ?>
.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_total_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_t_b, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_t_b, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->cart_total_padding_r_l, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->cart_total_padding_r_l, 'px !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .cart_totals tr.order-total th,
.fl-node-<?php echo $id; ?> .woocommerce .cart_totals tr.order-total td {
	border-bottom: none !important;
}
.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table tbody th,
.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table tbody td {
	<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_total_separator_width, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'border-top-style', 'solid' ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->cart_total_separator_color, ' !important' ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .cart_totals h2 {
	<?php WooPack_Helper::print_css( 'color', $settings->cart_totals_title_color, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .wc-proceed-to-checkout {
	<?php WooPack_Helper::print_css( 'text-align', $settings->checkout_align ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .wc-proceed-to-checkout a.button.alt {
	<?php WooPack_Helper::print_css( 'background-color', $settings->checkout_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->checkout_color ); ?>

	<?php if ( 'full_width' == $settings->checkout_width ) { ?>
		width: 100%;
	<?php } elseif ( 'custom' == $settings->checkout_width ) { ?>
		width: <?php echo $settings->checkout_width_custom; ?>%;
	<?php } ?>

	text-align: center;
}
.fl-node-<?php echo $id; ?> .woocommerce .wc-proceed-to-checkout a.button.alt:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->checkout_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->checkout_color_hover ); ?>
}

.fl-node-<?php echo $id; ?> .woocommerce .wc-proceed-to-checkout a.button.alt,
.fl-node-<?php echo $id; ?> .woocommerce button.button,
.fl-node-<?php echo $id; ?> .woocommerce input.button,
.fl-node-<?php echo $id; ?> .woocommerce a.remove,
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table.cart tbody td.product-name a {
	transition: all 0.3s ease-in-out;
}

.fl-node-<?php echo $id; ?> .woocommerce .quantity input.qty,
.woocommerce-page .fl-node-<?php echo $id; ?> .woocommerce .quantity input.qty {
	text-align: left;
	<?php WooPack_Helper::print_css( 'width', $settings->qty_width, 'px' ); ?>
}
<?php
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'qty_padding',
	'selector'		=> ".fl-node-$id .woocommerce .quantity input.qty, .woocommerce-page .fl-node-$id .woocommerce .quantity input.qty",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top'		=> 'qty_padding_top',
		'padding-right'		=> 'qty_padding_right',
		'padding-bottom'	=> 'qty_padding_bottom',
		'padding-left'		=> 'qty_padding_left',
	)
) );
?>

<?php
// *********************
// Media Query
// *********************
?>
@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_padding_t_b_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_padding_t_b_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->table_padding_r_l_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->table_padding_r_l_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart thead th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart td {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.cart .product-thumbnail img {
		<?php WooPack_Helper::print_css( 'width', $settings->image_width_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart td.product-remove a {
		<?php if ( 'custom' == $settings->product_remove_font_size && '' != $settings->product_remove_font_size_custom_medium ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom_medium, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_t_b_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_t_b_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->cart_total_padding_r_l_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->cart_total_padding_r_l_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table tbody th,
	.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table tbody td {
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_total_separator_width_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .wc-proceed-to-checkout a.button.alt {
		<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->checkout_width ); ?>
		<?php WooPack_Helper::print_css( 'width', $settings->checkout_width_custom_medium, '%', 'custom' == $settings->checkout_width ); ?>
	}
}
@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_padding_t_b_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_padding_t_b_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->table_padding_r_l_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->table_padding_r_l_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart thead th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart td {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.cart .product-thumbnail img {
		<?php WooPack_Helper::print_css( 'width', $settings->image_width_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart td.product-remove a {
		<?php if ( 'custom' == $settings->product_remove_font_size && '' != $settings->product_remove_font_size_custom_responsive ) { ?>
			<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom_responsive, 'px' ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_t_b_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_t_b_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->cart_total_padding_r_l_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->cart_total_padding_r_l_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table tbody th,
	.fl-node-<?php echo $id; ?> .woocommerce .cart_totals table.shop_table tbody td {
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_total_separator_width_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_total_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_total_padding_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .wc-proceed-to-checkout a.button.alt {
		<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->checkout_width ); ?>
		<?php WooPack_Helper::print_css( 'width', $settings->checkout_width_custom_responsive, '%', 'custom' == $settings->checkout_width ); ?>
	}
}

@media only screen and (max-width: 767px) {
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-cart-form table.shop_table.cart tbody tr:first-child td {
		<?php if ( '' != $settings->cart_item_border_width ) { ?>
			<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width, 'px' ); ?>
		<?php } else { ?>
			border-top-width: 1px;
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .quantity input.qty,
	.woocommerce-page .fl-node-<?php echo $id; ?> .woocommerce .quantity input.qty {
		margin: auto 0 0 auto;
	}
}