var woopack_<?php echo $id; ?> = '';
;(function($){

	$(document).ready(function() {
		woopack_<?php echo $id; ?> = new WooPackMiniCart({
			id: '<?php echo $id; ?>',
			behaviour: '<?php echo $settings->show_cart_on; ?>',
			isBuilderActive: <?php echo FLBuilderModel::is_builder_active() ? 'true' : 'false' ?>
		});
	});

})(jQuery);
